package com.example.myapplication

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

class MainApp : Application() {

    companion object{
        private lateinit var mInstance : MainApp
        fun get():MainApp{
            return  mInstance
        }
    }

    override fun onCreate() {
        super.onCreate()
        mInstance = this
    }
}