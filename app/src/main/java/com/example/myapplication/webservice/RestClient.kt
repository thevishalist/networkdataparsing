package com.example.myapplication.webservice

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RestClient {

    const val API_URL = "/posts"
    const val BASE_URL = "https://jsonplaceholder.typicode.com"

    fun getRetrofitClient(): Retrofit {
        val okHttpClient = OkHttpClient.Builder().build()
        return Retrofit.Builder().client(okHttpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build()
    }
}