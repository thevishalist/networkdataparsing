package com.example.myapplication.viewModels

import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.MainApp
import com.example.myapplication.webservice.RestClient
import com.example.myapplication.models.PostDataItem
import com.example.myapplication.webservice.ApiInterface
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserListViewModel : ViewModel() {

    val usersData = MutableLiveData<List<PostDataItem>?>()

    fun callApi() {
        val retrofit = RestClient.getRetrofitClient()
        val apiInterface = retrofit.create(ApiInterface::class.java)
        apiInterface.getUserData(RestClient.API_URL).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    usersData.postValue(parseUserListFromResponse(response))
                }else{
                    usersData.postValue(null)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                usersData.postValue(null)
                Handler(Looper.getMainLooper()).post {
                    Toast.makeText(MainApp.get(), t.message ?: "", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun parseUserListFromResponse(response: Response<ResponseBody>): List<PostDataItem>? {
        val responseInStr = response.body()?.string()
        if (!responseInStr.isNullOrEmpty()) {
            val gson = Gson()
            val type = object : TypeToken<List<PostDataItem>>() {}.type
            return gson.fromJson<ArrayList<PostDataItem>?>(responseInStr, type)
        }
        return null
    }

}