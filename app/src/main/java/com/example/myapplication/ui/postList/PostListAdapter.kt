package com.example.myapplication.ui.postList

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.MainApp
import com.example.myapplication.utils.ParcelKeys
import com.example.myapplication.R
import com.example.myapplication.databinding.RowItemUserBinding
import com.example.myapplication.models.PostDataItem
import com.example.myapplication.ui.postDetail.PostDetailActivity

class PostListAdapter(val mContext : Context) :
    RecyclerView.Adapter<PostListAdapter.UserItemViewHolder>() {

    private var mUsersList: List<PostDataItem> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserItemViewHolder {
        val binding =
            RowItemUserBinding.inflate(
                LayoutInflater.from(MainApp.get().applicationContext),
                parent,
                false
            )
        binding.root.tag = binding
        return UserItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: UserItemViewHolder, position: Int) {
        holder.bind(mUsersList[position])
    }

    override fun getItemCount() = mUsersList.size

    @SuppressLint("NotifyDataSetChanged")
    fun setNewDataList(newFeed: List<PostDataItem>) {
        mUsersList = newFeed
        notifyDataSetChanged()
    }

    inner class UserItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: PostDataItem) {
            val binding =
                itemView.findViewById<View>(R.id.parent_row_item).tag as RowItemUserBinding
            binding.user = item

            binding.parentRowItem.setOnClickListener {
                (mContext).startActivity(
                    Intent(
                        mContext,
                        PostDetailActivity::class.java
                    ).putExtra(ParcelKeys.PARCEL_KEY_POST_ITEM, item)
                )
            }
        }
    }

}