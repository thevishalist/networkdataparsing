package com.example.myapplication.ui.postDetail

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.myapplication.R
import com.example.myapplication.databinding.ActivityUserDetailBinding
import com.example.myapplication.models.PostDataItem
import com.example.myapplication.utils.ParcelKeys

class PostDetailActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityUserDetailBinding
    var mUserItem: PostDataItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_detail)

        supportActionBar?.apply {
            title = getString(R.string.title_detail)
            setDisplayHomeAsUpEnabled(true)
        }

        parseData()
        showDataOnUI()
    }

    private fun showDataOnUI() {
        if (mUserItem != null) {
            mBinding.item = mUserItem
        }
    }

    private fun parseData() {
        intent?.apply {
            mUserItem = getParcelableExtra(ParcelKeys.PARCEL_KEY_POST_ITEM)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }
}