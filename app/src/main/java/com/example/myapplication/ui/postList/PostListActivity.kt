package com.example.myapplication.ui.postList

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.MainApp
import com.example.myapplication.R
import com.example.myapplication.viewModels.UserListViewModel
import com.example.myapplication.databinding.ActivityMainBinding
import com.example.myapplication.models.PostDataItem

class PostListActivity : AppCompatActivity() {

    private lateinit var mUsersAdapter: PostListAdapter
    private lateinit var mBinding: ActivityMainBinding
    private lateinit var mViewModel: UserListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        supportActionBar?.apply {
            title = getString(R.string.title_list)
        }

        setupViewModel()
        setupRecycler()
        mViewModel.callApi()
    }

    private fun setupViewModel() {
        mViewModel = ViewModelProvider(this)[UserListViewModel::class.java]
        mViewModel.usersData.observe(this, {
            if (it != null && it.isNotEmpty()) {
                mBinding.tvErr.visibility = View.GONE
                feedDataInRecycler(it)
            } else {
                mBinding.tvErr.visibility = View.VISIBLE
            }
        })
    }

    private fun setupRecycler() {
        if (!::mUsersAdapter.isInitialized) {
            mUsersAdapter = PostListAdapter(this)
        }
        mBinding.recyclerView.addItemDecoration(
            DividerItemDecoration(
                MainApp.get(),
                RecyclerView.HORIZONTAL
            )
        )
        mBinding.recyclerView.adapter = mUsersAdapter
    }

    private fun feedDataInRecycler(dataFeed: List<PostDataItem>) {
        mUsersAdapter.setNewDataList(dataFeed)
    }

}